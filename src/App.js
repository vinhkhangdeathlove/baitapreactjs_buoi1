import "./App.css";
import Header from "./Component/Header/Header";
import Footer from "./Component/Footer/Footer";
import Banner from "./Component/Body/Banner/Banner";
import Item from "./Component/Body/Item/Item";

function App() {
  return (
    <div className="App">
      <Header />
      <Banner />
      <Item />
      <Footer />
    </div>
  );
}

export default App;
